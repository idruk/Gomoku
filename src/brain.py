from random import randint
import sys
import numpy

class Brain:

    def __init__(self):
        self.patherns = [
            "11110", "11101", "11011", "10111", "01111",
            "22220", "22202", "22022", "20222", "02222",
            "11100", "11010", "11001", "10110", "10101", "10011", "01110", "01101", "01011", "00111",
            "22200", "22020", "22002", "20220", "20202", "20022", "02220", "02202", "02022", "00222",
            "11000", "10100", "10010", "10001", "01100", "01010", "01001", "00110", "00101", "00011",
            "10", "01"
        ]
        # self.patherns = ["11110","01111", "10111", "11011", "11101", "22220",
        #                  "02222", "20222", "22022", "22202" , "20202",  "01110",
        #  "1011", "001112", "010112", "011012", "10011", "10101", "0222", "2220", "2011102",
        #  "00110", "01010", "010010","000112", "001012", "010012", "10001",
        #  "2010102", "2011002", "011", "110" , "01", "10"]
        self.map = []
        self.found = []

    def retriveMap(self, map):
        self.map = map

    def checkVertical(self):
        mapsize = len(self.map)
        movPower = len(self.patherns) + 1
        xRet = 0
        yRet = 0

        for i in range(mapsize):
            tmpBorad = [row[i] for row in self.map]
            tmp = ""
            for char in tmpBorad:
                tmp += str(char)
            for partern in self.patherns:
                ret = tmp.find(partern)
                if (ret != -1):
                    if self.patherns.index(partern) < movPower:
                        movPower = self.patherns.index(partern)
                        xRet = ret + partern.find("0")
                        yRet = i
                    # print("vertical match with: " + partern)
                    # print("playing on ->   x: " + str(ret + partern.find("0")) + " y : " + str(i) + " with power: " + str(movPower))
        return xRet, yRet, movPower

    def checkHorizontal(self):
        movPower = len(self.patherns) + 1
        x = 0
        xRet = 0
        yRet = 0
        
        for i in self.map:
            tmp = ""
            for char in i:
                tmp += str(char)
            for partern in self.patherns:
                ret = tmp.find(partern)
                if (ret != -1):
                    if self.patherns.index(partern) < movPower:
                        movPower = self.patherns.index(partern)
                        xRet = x
                        yRet = ret + partern.find("0")
                        # print("Horizontal match with: " + partern)
                        # print("playing on ->   x: " + str(x) + " y : " + str(ret + partern.find("0")) + " with power: " + str(movPower))
                    # return x, (ret + partern.find("0"))
            x += 1
        return xRet, yRet, movPower

    def checkdiagonalRight(self):
        test = numpy.matrix(self.map)
        movPower = len(self.patherns) + 1
        xRet = 0
        yRet = 0

        for i in range(-(len(self.map) - 5), len(self.map) - 4):
            tmp = ""
            for char in numpy.diag(test, i):
                tmp += str(char)
            for patern in self.patherns:
                ret = tmp.find(patern)
                if (ret != -1):
                    if i >= 0:
                        y = i
                    if i < 0:
                        y = -i
                    x = 0
                    for ind in range(0, (ret + patern.find("0"))):
                        x += 1
                        y += 1
                    if self.patherns.index(patern) < movPower:
                        movPower = self.patherns.index(patern)
                        xRet = x
                        yRet = y
                    # print("Diagonal match with: " + patern)
                    # print("playing on ->   x: " + str(x)+ " y: " + str(y) + " with power: " + str(movPower))
        return xRet, yRet, movPower
    
    def checkdiagonalLeft(self):
        test = numpy.matrix(self.map)
        movPower = len(self.patherns) + 1
        xRet = 0
        yRet = 0

        for i in range(-(len(self.map) - 5), len(self.map) - 4):
            tmp = ""
            for char in numpy.diag(numpy.fliplr(test), i):
                tmp += str(char)
            for patern in self.patherns:
                ret = tmp.find(patern)
                if (ret != -1):
                    if i >= 0:
                        y = (len(self.map) - 1) - i
                        x = 0
                    if i < 0:
                        y = (len(self.map) - 1)
                        x = (-i)
                    for ind in range(0, (ret + patern.find("0"))):
                        x += 1
                        y -= 1
                    if self.patherns.index(patern) < movPower:
                        movPower = self.patherns.index(patern)
                        xRet = x
                        yRet = y
                    # print("Diagonal match with: " + patern)
                    # print("playing on ->   x: " + str(x)+ " y: " + str(y) + " with power: " + str(movPower))
        return xRet, yRet, movPower

    def findPathern(self, map):
        self.retriveMap(map)
        xRet = -1
        xTmp = -1
        yRet = -1
        yTmp = -1
        movPowerTmp = len(self.patherns) + 1
        movPower = len(self.patherns) + 1

        xTmp, yTmp, movPowerTmp = self.checkHorizontal()
        if movPowerTmp < movPower:
            movPower = movPowerTmp
            xRet = xTmp
            yRet = yTmp
        xTmp, yTmp, movPowerTmp = self.checkVertical()
        if movPowerTmp < movPower:
            movPower = movPowerTmp
            xRet = xTmp
            yRet = yTmp
        xTmp, yTmp, movPowerTmp = self.checkdiagonalRight()
        if movPowerTmp < movPower:
            movPower = movPowerTmp
            xRet = xTmp
            yRet = yTmp
        xTmp, yTmp, movPowerTmp = self.checkdiagonalLeft()
        if movPowerTmp < movPower:
            movPower = movPowerTmp
            xRet = xTmp
            yRet = yTmp
        if (movPower == (len(self.patherns) + 1)):
            while (1):
                xRet = randint(0, len(self.map) - 1)
                yRet = randint(0, len(self.map) - 1)
                if self.map[xRet][yRet] == 0:
                    break
        print(str(yRet) + "," + str(xRet))
        sys.stdout.flush()
        # print("top mov is  -> x: ", str(xRet) + " y: ", str(yRet) + "  with power of: " + str(movPower))
        return (xRet, yRet, movPower)
        

# if __name__ == "__main__":
#     map = [                             # x
#         [8, 0, 0, 0, 0, 0, 0, 0, 0, 9], # 0
#         [0, 0, 2, 2, 2, 0, 1, 0, 0, 0], # 1
#         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], # 2
#         [0, 0, 0, 0, 1, 0, 0, 0, 0, 0], # 3
#         [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], # 4
#         [0, 0, 0, 0, 0, 0, 0, 0, 0, 1], # 5
#         [0, 2, 0, 0, 0, 0, 3, 0, 0, 0], # 6
#         [0, 0, 2, 0, 0, 3, 0, 0, 0, 1], # 7
#         [0, 0, 0, 2, 3, 0, 0, 0, 0, 1], # 8
#         [9, 0, 0, 0, 0, 0, 0, 0, 0, 8]  # 9
#     # y  0  1  2  3  4  5  6  7  8  9
#     ]
#     x = Brain()
#     x.findPathern(map)
