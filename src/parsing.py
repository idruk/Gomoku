#!/usr/bin/python3

import sys
from brain import Brain
from random import seed
from random import randint

class Parsing:

    def __init__(self):
        self.commands = [
            self.start_interpretation, self.turn_interpretation,
            self.end_interpretation, self.about_interpretation,
            self.begin_interpretation, self.board_command
        ]
        self.board_size = 0
        self.board = []
        self.initialisedBoard = False
        self.begin = False
        self.brain = Brain()
        seed(1)

    ## start, must be sent for init board
        # Example:
        # The manager sends:
        # START 20
        # The brain answers:
        # OK - everything is good
        # ERROR message - unsupported size or other error
    def start_interpretation(self, inp):
        try:
            cmd = inp.split(' ')[0]
            if (cmd == 'START'):
                try:
                    size = inp.split(' ')[1]
                    self.board_size = int(size)
                    if self.board_size <= 0:
                        sendMessages('ERROR message - unsupported size or other error')
                        return
                    for y in range(0, self.board_size):
                        tmp = []
                        for i in range(0, self.board_size):
                            tmp.append(0)
                        self.board.append(tmp)
                    self.initialisedBoard = True
                    sendMessages ("OK")
                except:
                    sendMessages('ERROR message - unsupported size or other error')
                    pass
        except:
            pass

    ## turn, must be sent to play
        # Example:
        # The manager sends:
        # TURN 10,10
        # The brain answers:
        # 11,10
    def turn_interpretation(self, inp):
        try:
            cmd = inp.split(' ')[0]
            if (cmd == 'TURN'):
                if (self.initialisedBoard) == False:
                    sendMessages('ERROR message - uninitialised board')
                    pass
                try:
                    cmd, coords = inp.split(' ')
                    x_in, y_in = coords.split(',')
                    x = int(x_in)
                    y = int(y_in)
                    if x > self.board_size or y > self.board_size or x < 0 or y < 0:
                        sendMessages ('ERROR message - unsupported')
                        pass
                    else:
                        if self.board[y][x] == 0:
                            self.begin = True
                            self.board[y][x] = 2
                            y, x, power = self.brain.findPathern(self.board)
                            self.board[y][x] = 1
                        else:
                            print('ERROR message - unsupported turn')
                        # for x in self.board:
                        #     print(x, end = ' ')
                        #     print(" # " + str(self.board.index(x)))
                        # print(" 0  1  2  3  4  5  6  7  8  9")
                except:
                    sendMessages ('ERROR message - unsupported turn')
                    pass
        except:
            pass

    ## end, exyt the program
        # Expected answer: nonex    
    def end_interpretation(self, inp):
        if (inp == 'END'):
            sys.exit(0)

    ## about, send infos
        # Example:
        # The manager sends:
        # ABOUT
        # The brain answers:
    def about_interpretation(self, inp):
        if (inp == "ABOUT"):
            sendMessages("name=\"Unamed\", version=\"1.0\", author=\"Idruk\", country=\"France\"")

    def begin_interpretation(self, s_input):
        if (s_input == 'BEGIN'):
            if (self.initialisedBoard == False):
                sendMessages('ERROR message - uninitialised board')
                return
            if (self.begin == True):
                sendMessages('ERROR message - already begone')
            self.begin = True
            y, x, power = self.brain.findPathern(self.board)
            self.board[y][x] = 1
            # for x in self.board:
            #     print(x)
            
    
    def board_command(self, s_input):
        if (s_input == 'BOARD'):
            if self.initialisedBoard == False:
                sendMessages("ERROR message - game not started")
            else:
                for x in range(self.board_size):
                    for y in range(self.board_size):
                        self.board[x][y] = 0
                coord = ''
                coord = input()
                while coord != 'DONE':
                    x, y, player = coord.split(',')
                    self.board[int(y)][int(x)] = int(player)
                    coord = input()
            y, x, power = self.brain.findPathern(self.board)
            self.board[y][x] = 1

    def get_command(self):
        s_input = input()
        for i in self.commands:
            i(s_input)


    def start_function(self):
        while (1):
            try:
                self.get_command()
            except:
                sys.exit(0)

def sendMessages(msg):
    print(msg)
    sys.stdout.flush()
