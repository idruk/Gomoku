##
## EPITECH PROJECT, 2019
## ma
## File description:
## ma
##

all:
	cp src/parsing.py .
	cp src/brain.py .
	cp src/main.py ./pbrain-gomoku-ai
	chmod 755 ./pbrain-gomoku-ai

clean:
	rm -f pbrain-gomoku-ai parsing.py brain.py

fclean: clean

re: fclean all